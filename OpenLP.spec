%{!?python_sitelib:%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: Open source Church presentation and lyrics projection application
Name: OpenLP
Version: 2.0.3
Release: 1%{?dist}
Source0: http://downloads.sourceforge.net/openlp/openlp/%{version}/%{name}-%{version}.tar.gz
License: GPLv2
Group: Applications/Multimedia
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

URL: http://openlp.org/

BuildRequires:  desktop-file-utils
BuildRequires:  python2-devel
BuildRequires:  python-setuptools

Requires:       PyQt4
Requires:       phonon
Requires:       python-BeautifulSoup
Requires:       python-chardet
Requires:       python-lxml
Requires:       python-sqlalchemy
Requires:       python-enchant
Requires:       python-mako
Requires:       python-openoffice
Requires:       python-migrate
Requires:       hicolor-icon-theme
Requires:       libreoffice-graphicfilter
Requires:       libreoffice-impress
Requires:       libreoffice-headless

%description
OpenLP is a church presentation software, for lyrics projection software,
used to display slides of Songs, Bible verses, videos, images, and
presentations via LibreOffice using a computer and projector.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --skip-build -O1 --root %{buildroot}

install -m644 -p -D resources/images/openlp-logo-16x16.png \
   %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/openlp.png
install -m644 -p -D resources/images/openlp-logo-32x32.png \
   %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/openlp.png
install -m644 -p -D resources/images/openlp-logo-48x48.png \
   %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/openlp.png
install -m644 -p -D resources/images/openlp-logo.svg \
   %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/openlp.svg

desktop-file-install \
  --dir %{buildroot}/%{_datadir}/applications \
  resources/openlp.desktop

desktop-file-validate %{buildroot}/%{_datadir}/applications/openlp.desktop

mv %{buildroot}%{_bindir}/openlp.pyw %{buildroot}%{_bindir}/openlp

mkdir -p %{buildroot}%{_datadir}/openlp/i18n/
mv resources/i18n/*.qm %{buildroot}%{_datadir}/openlp/i18n
mkdir -p %{buildroot}%{_datadir}/mime/packages
cp -p resources/openlp.xml %{buildroot}%{_datadir}/mime/packages

%post
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
update-mime-database %{_datadir}/mime &> /dev/null ||:
update-desktop-database &> /dev/null ||:

%postun
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
update-mime-database %{_datadir}/mime &> /dev/null ||:
update-desktop-database &> /dev/null ||:

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc copyright.txt LICENSE
%{_bindir}/openlp
%{_datadir}/mime/packages/openlp.xml
%{_datadir}/applications/openlp.desktop
%{_datadir}/icons/hicolor/*/apps/openlp.*
%{_datadir}/openlp
%{python_sitelib}/openlp/
%{python_sitelib}/OpenLP-%{version}*.egg-info


%changelog
* Sat Sep 14 2013 Tim Bentley <timbentley@openlp.org> - 2.0.3-1
- Release 2.0.3

* Sun Aug 25 2013 Tim Bentley <timbentley@openlp.org> - 2.0.2-2
- Release 2.0.2

* Fri Aug 02 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Jan 05 2013 Tim Bentley <timbentley@openlp.org> - 2.0.1-1
- Release 2.0.1

* Sat Dec 1 2012 Tim Bentley <timbentley@openlp.org> - 2.0-1
- Release 2.0

* Sat Sep 15 2012 Tim Bentley <timbentley@openlp.org> - 1.9.12-1
- Release 1.9.12

* Sat Jul 28 2012 Tim Bentley <timbentley@openlp.org> - 1.9.11-1
- Relaese 1.9.11 build

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.10-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jun 23 2012 Tim Bentley <timbentley@openlp.org> - 1.9.10-2
- Import Issues

* Sat Jun 23 2012 Tim Bentley <timbentley@openlp.org> - 1.9.10-1
- Release 1.9.10 build

* Sun Mar 25 2012 Tim Bentley <timbentley@openlp.org> - 1.9.9-1
- Release 1.9.9 build

* Fri Dec 23 2011 Tim Bentley <timbentley@openlp.org> - 1.9.8-1
- Release 1.9.8 build

* Sat Sep 24 2011 Tim Bentley <timbentley@openlp.org> - 1.9.7
- Weekly build

* Sat Jun 25 2011 Tim Bentley <timbentley@openlp.org> - 1.9.6-1
- Release build for beta 2

* Sat Jun 11 2011 Tim Bentley <timbentley@openlp.org> - 1.9.5-5
- Update Build Script

* Tue May 24 2011 Tim Bentley <timbentley@openlp.org> - 1.9.5-4
- Fix Typing error

* Mon May 23 2011 Tim Bentley <timbentley@openlp.org> - 1.9.5-3
- Fix dependancy

* Mon May 23 2011 Tim Bentley <timbentley@openlp.org> - 1.9.5-2
- Fix LibreOffice addition for presentations

* Fri Mar 25 2011 Tim Bentley <timbentley@openlp.org> - 1.9.5-1
- Beta 1 release build

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jan 8 2011 Tim Bentley <timbentley@openlp.org> - 1.9.4-1
- Alpha 4 release build

* Sat Sep 25 2010 Tim Bentley <timbentley@openlp.org> - 1.9.3-1
- Alpha 3 release build

* Mon Aug 30 2010 Tim Bentley <timbentley@openlp.org> - 1.9.2.1-4
- Update to build 1000

* Sat Aug 28 2010 Tim Bentley <timbentley@openlp.org> - 1.9.2.1-3
- Update to 996 and test build on git

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 1.9.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Sat Jun 26 2010 Tim Bentley <timbentley@openlp.org> 1.9.2.1
- Fix bug with build versions

* Sat Jun 26 2010 Tim Bentley <timbentley@openlp.org> 1.9.2
- New Release - Alpha 2 Release

* Sun Mar 28 2010 Tim Bentley <timbentley@openlp.org> 1.9.1.1
- Initial build version - Alpha 1 Release
